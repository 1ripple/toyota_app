var gulp = require('gulp');
// var del = require('del');
var runSequence = require('run-sequence');
var $ = require('gulp-load-plugins')({
	pattern: ['gulp-*', 'gulp.*'],
	replaceString: /\bgulp[\-.]/
});

var cache = require('gulp-cached'); //<--


//Configファイル読み込み
var config = require('./gulp/config');

//taskload(使用するtask読み込み)
require('./gulp/tasks/load');
/**----------------------------------------
 * Scss
 ----------------------------------------*/
gulp.task('sass', function () {
	gulp.src([config.path.scss + '**/*.scss'])
	//gulp.src([config.path.scss + '**/*.scss','!'+config.path.scss + 'pc_index.scss'])//PCファイルは除外
		.pipe($.plumber({errorHandler: $.notify.onError("<%= error.message %>")}))

		.pipe($.compass({
			config_file: "config.rb",
			comments: false,
			css: config.path.css,
			sourcemap: false,//sassバージョン3.3.3以降
			sass: config.path.scss
		}))
		.pipe($.pleeease({
			fallbacks: {
				// autoprefixer: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']
				autoprefixer: ['last 1 versions','ie >= 10','safari >= 8','ios >= 8','android >= 4']
			},
			optimizers: {
				minifier: false, //　圧縮の有無
				mqpacker: true
			}
		}))
		.pipe($.changed(config.path.css,{extention:'.css'}))
		// .pipe(gulp.dest(config.path.css));
});

/**----------------------------------------
 * Pug
 ----------------------------------------*/
gulp.task("pug", function() {
	gulp.src(
		[config.path.pug+"**/*.pug",'!' + config.path.pug+"**/_*.pug"]//参照するディレクトリ、出力を除外するファイル
	)
	.pipe($.plumber({errorHandler: $.notify.onError("<%= error.message %>")}))
	.pipe($.pug({
		basedir: 'html/pug',
		pretty: true
	}))
	.pipe(gulp.dest(config.path.htmldest))//出力先
});


/**----------------------------------------
 * Webpack
 ----------------------------------------*/
///// webpack /////
var webpackStream = require("webpack-stream");
var webpack = require("webpack");
// webpackの設定ファイルの読み込み
var webpackConfig = require("./webpack.config");

///////////////////
// gulp.task('webpack', function () {
//   // ☆ webpackStreamの第2引数にwebpackを渡す☆
//   return webpackStream(webpackConfig, webpack)
//     .pipe(gulp.dest(config.path.js));
// });

gulp.task('coffeeWebpack', function () {
	gulp.src(config.path.coffee+'**/*.coffee')
		.pipe($.plumber({errorHandler: $.notify.onError("<%= error %>")}))
		.pipe($.webpack(require('./webpack.config.js')))  // webpack.config.jsの指定
		.pipe(gulp.dest(config.path.js))
});

gulp.task('babelWebpack', function () {
	gulp.src(['es6/**/*.js',config.path.coffee+'**/*.coffee'])
		.pipe($.plumber({errorHandler: $.notify.onError("<%= error %>")}))
		.pipe($.webpack(require('./webpack.config.js')))  // webpack.config.jsの指定
		.pipe(gulp.dest(config.path.js))
});


/**----------------------------------------
 * Watch
 ----------------------------------------*/
// 各ファイルの監視
gulp.task('watch', function () {
	gulp.watch([config.path.scss + '**/*.scss'], ['sass']);//scssの場合
	gulp.watch([config.path.scss + '**/*.sass'], ['sass']);//sassの場合
	gulp.watch([config.path.coffee + '**/*.coffee'], ['coffeeIndex','coffeeSp']);
});

gulp.task('watchSp', function () {
	gulp.watch([config.path.scss + 'sp/*.scss'], ['cssSp']);
	gulp.watch([config.path.scss + 'sp/*.sass'], ['cssSp']);
	gulp.watch([config.path.coffee + '**/*.coffee'], ['coffeeSp']);
});
/**
 * watch es6/babel
 */
//es6のみ監視
gulp.task('watch_es6', function () {
	gulp.watch(['es6/**/*.js'], ['babel']);
});

//js+cooffee+scss+livereload
gulp.task('watch-reload', function () {
	//gul-watchを使用に変更180618
	/*** css ***/
	//sass,scss
	gulp.watch([config.path.scss + '**/*.scss',config.path.scss + '**/*.sass'],  () => {
    return gulp.start(['sass'])
  });
	//↓coffeeでの開発の場合
	// gulp.watch([config.path.coffee + '**/*.coffee'], ['coffeeIndex']);
	//↓es6での開発の場合
	//gulp.watch([config.path.es6 + '**/*.js'], ['babel']);//babel Concat開発
	// gulp.watch([config.path.es6 + '**/*.js'], ['babelWebpack']);//webpack開発
	//gulp.watch([config.path.es6 + '**/*.js',config.path.coffee + '**/*.coffee'], ['babelWebpack']);//webpack開発

	/*** js ***/
	gulp.watch([config.path.es6 + '**/*.js',config.path.coffee + '**/*.coffee'], () => {//webpack開発
    return gulp.start(['babelWebpack'])
	});


	/*** html ***/
	//ejs//
	//gulp.watch([config.path.ejs+"**/*.ejs"],  () => {return gulp.start(['ejs'])});
	//pug//
	gulp.watch([config.path.pug+"**/*.pug"],  () => {return gulp.start(['pug'])});

	//reload//
	// gulp.watch(reloadList, ["reload-html"]);
	gulp.watch(reloadList, () => {
    return gulp.start(['reload-html'])
  });
});


//Webサーバー
 gulp.task('webserver', function() {
  gulp.src(config.destRoot) //Webサーバーで表示するサイトのルートディレクトリを指定
    .pipe($.webserver({
			host: 'localhost',
      livereload: true, //ライブリロードを有効に
      port: 8001
      //directoryListing: true //ディレクトリ一覧を表示するか。オプションもあり
    }));
});

gulp.task('connectDev', function () {
	$.connect.server({
    root      :config.destRoot,
    port      :8000,
    livereload:true
  });
});



/**----------------------------------------
 * LIVE RELOAD
 ----------------------------------------*/
var reloadList = [
	/*
	config.path.css+"index.css",
	config.path.js+"index.js",
	config.destRoot+"index.html"
	*/
	//↓全て
	config.destRoot+'/**/*.html',
	config.path.js+'/**/*.js',
	config.path.css+'/**/*.css'
]

gulp.task('reload-html', function () {
	gulp.src(config.destRoot+'/**/*.html').pipe($.connect.reload());
});

/**----------------------------------------
 * uglify
 ----------------------------------------*/
gulp.task('js-min', function () {
	gulp.src(config.path.js+'**/*.js')
		.pipe($.uglify({preserveComments:"some"}))
		.pipe(gulp.dest(config.path.js));
});
gulp.task('css-min', function () {
	gulp.src(config.path.css+'**/*.css')
		.pipe($.cssmin())
		.pipe(gulp.dest(config.path.css));
	//.pipe(gulp.dest(config.path.css + 'index/'));
});


//　defaultで行うストリーム
gulp.task('default', function(cb) {
	// runSequence(['connectDev', 'watch-reload']);
	 runSequence(['webserver', 'watch-reload']);
	// runSequence(['webserver', 'watch_sass'],cb);
});

//　js,css圧縮
gulp.task('min', function(cb) {
	runSequence(['js-min', 'css-min']);
});
