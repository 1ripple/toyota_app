
var destRoot = "../htdocs/";
var destSpRoot = "../htdocs/sp/assets/";

var sourcePath = "";

module.exports = {
  destRoot:destRoot,
  destSpRoot:destSpRoot,


  path: {
    js: destRoot+'assets/js/',
    css: destRoot+'assets/css/',
    image: destRoot+'assets/images/',
    libdest: destRoot+'assets/js/lib/',
    htmldest: destRoot,

    coffee: sourcePath+'coffee/',
    es6: sourcePath+'es6/',
    libroot: sourcePath+'javascripts/lib/',
    scss: sourcePath+'sass/',
    ejs: sourcePath+'html/ejs/',
    pug: sourcePath+'html/pug/'
  }
};