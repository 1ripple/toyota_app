var gulp = require('gulp');
var config = require('../config');

var $ = require('gulp-load-plugins')({
	pattern: ['gulp-*', 'gulp.*'],
	replaceString: /\bgulp[\-.]/
});


/**----------------------------------------
 * JS 結合
 ----------------------------------------*/
var jsList = {
  // CONCAT...
  main:[
        "javascripts/wc/script.js"
  ]
}

gulp.task('js-concat', function() {
 var arr, name;
 for (name in jsList) {
  arr = jsList[name];
  gulp.src(arr).pipe($.concat(""+name+".js")).pipe(gulp.dest(config.path.js));
 }
});
/**----------------------------------------
* CSS 結合
----------------------------------------*/
var cssList = {
  // CONCAT...
  index:[
    config.path.css+"base/index.css"
  ]

}
gulp.task('css-concat', function() {
 var arr, name;
 for (name in cssList) {
   arr = cssList[name];
   gulp.src(arr).pipe($.concat("" + name + ".css")).pipe(gulp.dest(config.path.css));
 }
});