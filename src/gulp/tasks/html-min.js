
var gulp = require('gulp');
var config = require('../config');

var $ = require('gulp-load-plugins')({
	pattern: ['gulp-*', 'gulp.*'],
	replaceString: /\bgulp[\-.]/
});

gulp.task('html-min', () => {
  return gulp.src(config.destRoot+'**/*.html')
    .pipe($.htmlmin({
        // 余白を除去する
        collapseWhitespace : true,
        // HTMLコメントを除去する
        removeComments : true
    }))
    .pipe(gulp.dest(config.destRoot))
})