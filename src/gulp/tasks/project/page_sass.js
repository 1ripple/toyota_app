var gulp = require('gulp');
var config = require('../../config');

var $ = require('gulp-load-plugins')({
	pattern: ['gulp-*', 'gulp.*'],
	replaceString: /\bgulp[\-.]/
});

//ページごとにsass単体監視
var pageFileName = "index.scss";

gulp.task('cssPage', function () {
	gulp.src(config.path.scss + pageFileName)
		.pipe($.plumber({errorHandler: $.notify.onError("<%= error.message %>")}))
		.pipe($.compass({
			config_file: "config.rb",
			css: config.path.css,
			sourcemap: false,
			sass: config.path.scss
		}))
		.pipe($.pleeease({
			fallbacks: {
				autoprefixer: ['last 1 versions','ie >= 10','safari >= 8','ios >= 8','android >= 4']
			},
			optimizers: {
				minifier: false, //　圧縮の有無
				mqpacker: true
			}
		}))
    .pipe(gulp.dest(config.path.css));

    //gulp.watch([config.path.scss + '**/*.scss'], ['sass']);//scssの場合
    
});


/**
 * watch scss
 */
//scssのみ監視
gulp.task('watch_sass', function () {
	gulp.watch([config.path.scss + '**/*.scss',config.path.scss + '**/*.sass'], ['cssPage']);
	gulp.watch(config.path.css+'/**/*.css', () => {
    return gulp.start(['reload-html'])
  });
});