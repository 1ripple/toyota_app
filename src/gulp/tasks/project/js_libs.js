var gulp = require('gulp');
var config = require('../../config');

var $ = require('gulp-load-plugins')({
	pattern: ['gulp-*', 'gulp.*'],
	replaceString: /\bgulp[\-.]/
});

/**----------------------------------------
 * jsライブラリ統合
 ----------------------------------------*/
 gulp.task('libs', function () {
	gulp.src([
		config.path.libroot + 'jquery-3.1.1.min.js',
		// config.path.libroot + 'jquery-2.2.4.min.js',
		// config.path.libroot + 'jquery-1.12.4.min.js',
		config.path.libroot + 'greensock/TweenMax.min.js'
		//config.path.libroot + 'ScrollMagic.min.js',
		//config.path.libroot + 'debug.addIndicators.min.js',
		//config.path.libroot + 'preloadjs-0.6.2.min.js',
		//config.path.libroot + 'easeljs-0.8.2.min.js',
		//config.path.libroot + 'soundjs-0.6.2.min.js',
		//config.path.libroot + 'slick.min.js'
		])
		.pipe($.plumber({errorHandler: $.notify.onError('<%= error.message %>')}))
		.pipe($.concat('libs.js'))
		.pipe($.uglify({preserveComments:"some"}))//圧縮
		.pipe(gulp.dest(config.path.libdest));
});

gulp.task('libsSp', function () {
	gulp.src([
		config.path.libroot + 'jquery-3.1.1.min.js',
		config.path.libroot + 'greensock/TweenMax.min.js'
		])
		.pipe($.plumber({errorHandler: $.notify.onError('<%= error.message %>')}))
		.pipe($.concat('libs.js'))
		.pipe($.uglify({preserveComments:"some"}))//圧縮
		.pipe(gulp.dest(config.destSpRoot+'js/'));
});