var gulp = require('gulp');
var config = require('../config');

var $ = require('gulp-load-plugins')({
	pattern: ['gulp-*', 'gulp.*'],
	replaceString: /\bgulp[\-.]/
});

/**----------------------------------------
 * ejs
 ----------------------------------------*/

gulp.task("ejs", function() {
  gulp.src(
      [config.path.ejs+"**/*.ejs",'!' + config.path.ejs+"**/_*.ejs"]
  )
    .pipe($.plumber({errorHandler: $.notify.onError("<%= error %>")}))
    .pipe($.ejs({},{},{ext:'.html'}))//バージョン3以上
    .pipe(gulp.dest(config.path.htmldest))
});