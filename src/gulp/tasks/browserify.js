var gulp = require('gulp');
var config = require('../config');

///// browserify /////
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
///////////////////

/**----------------------------------------
 * Browserify
 ----------------------------------------*/

 gulp.task('browserify', function() {
  browserify(config.path.es6 +'Index.js', { debug: true })
    .transform(babelify)
    .bundle()
    .on("error", function (err) { console.log("Error : " + err.message); })
    .pipe(source("app.js"))
    .pipe(gulp.dest(config.path.js))
});
