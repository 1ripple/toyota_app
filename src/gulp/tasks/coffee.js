var gulp = require('gulp');
var config = require('../config');

var $ = require('gulp-load-plugins')({
	pattern: ['gulp-*', 'gulp.*'],
	replaceString: /\bgulp[\-.]/
});


/**----------------------------------------
 * Coffee
 ----------------------------------------*/
 gulp.task('coffeeIndex', function () {
	gulp.src([
			config.path.coffee + 'wolk/event/EventDispatcher.coffee',
			config.path.coffee + 'wolk/api/YoutubeApi.coffee',
			config.path.coffee + 'index.coffee'
		])
		//.pipe(cache( 'coffee' ))//変更ファイルのみ
		.pipe($.plumber({errorHandler: $.notify.onError("<%= error %>")}))
		.pipe($.coffee({
			bare: true
		}))
		.pipe($.concat('index.js'))
		.pipe(gulp.dest(config.path.js))
});

gulp.task('coffeeSp', function () {
	//console.log('changed coffeeSp: ');
	gulp.src([
			config.path.coffee + 'sp/SpIndex.coffee'

		])
		//.pipe(cache( 'coffee' ))//変更ファイルのみ
		.pipe($.plumber({errorHandler: $.notify.onError("<%= error %>")}))
		.pipe($.coffee({
			bare: true
		}))
		.pipe($.concat('index.js'))
		.pipe(gulp.dest(config.destSpRoot+'js/'))
});


/**
 * watch coffee
 */
//coffeeのみ監視
gulp.task('watch_coffee', function () {
	gulp.watch([config.path.coffee + '**/*.coffee'], ['coffeeIndex']);
});