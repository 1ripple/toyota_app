var gulp = require('gulp');
var config = require('../config');

var $ = require('gulp-load-plugins')({
	pattern: ['gulp-*', 'gulp.*'],
	replaceString: /\bgulp[\-.]/
});

///// babel /////
var babel = require("gulp-babel");
/////////////////

//babel Concat開発
//(gulpのみでwebpack未使用の場合)
/**----------------------------------------
 * Babel
 ----------------------------------------*/

 gulp.task('babel', function() {
  //  gulp.src('es6/**/*.js')
	 gulp.src([
 			config.path.es6 + 'wolk/event/EventEmitter.js',
 		// 	config.path.es6 + 'wolk/window/ScrlController.js',
 		// 	config.path.es6 + 'wolk/api/YoutubePlayer.js',
 		// 	config.path.es6 + 'wolk/carousel/SlideFlex.js',
 			config.path.es6 + 'Index.js'
 		])
		.pipe($.plumber({errorHandler: $.notify.onError('<%= error.message %>')}))
		.pipe($.eslint({useEslintrc: true}))
		// .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failOnError())

		.pipe($.concat('index_babel.js'))
    .pipe(babel())

    .pipe(gulp.dest(config.path.js))
 });