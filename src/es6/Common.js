
import CommonUi from "es6/module/CommonUi";
import SceneManager from "es6/module/SceneManager";

$(document).ready(function() {

  CommonUi.getInstance().init();
  SceneManager.getInstance().init();
});



