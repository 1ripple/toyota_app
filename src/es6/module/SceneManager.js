
let _instance = null;

export default class SceneManager {
  // static evt = null;
  prevPageID = "home";
  currentPageID = "home";
  constructor() {
    if ( _instance !== null ) {
      throw new Error('SingleTon.instance()');
    }
    if ( _instance === null ) {
        _instance = this;
    }

    return _instance;
  }
  static getInstance() {
    if ( _instance === null ) {
      _instance = new SceneManager();
    }
    return _instance;
  }

  init(){

  }

  changePage(pageID){
    this.prevPageID = this.currentPageID;
    this.currentPageID = pageID;

    TweenMax.to('.scene', 0.3, {display:"none",alpha:0, ease:Sine.easeOut,delay:0});
    TweenMax.to('#'+pageID, 0.5, {alpha:1,display:"block", ease:Sine.easeOut,delay:0.3});
  }

  clickedHomeThumb(){
    TweenMax.to('#home', 0.3, {display:"none",alpha:0, ease:Sine.easeOut,delay:0});
    TweenMax.to('#article', 0.5, {alpha:1,display:"block", ease:Sine.easeOut,delay:0.3});
  }


}