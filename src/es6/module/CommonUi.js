import SceneManager from "es6/module/SceneManager";

let _instance = null;
export default class CommonUi {

  constructor() {
    if ( _instance !== null ) {
      throw new Error('SingleTon.instance()');
    }
    if ( _instance === null ) {
        _instance = this;
    }

    return _instance;
  }
  static getInstance() {
    // this._this = this;
    if ( _instance === null ) {
      _instance = new CommonUi();
    }
    return _instance;
  }

  init(){
    this.setUpMainNavi();
  }
  setUpMainNavi(){
    $("#mn-home").on('click',()=> {
      SceneManager.getInstance().changePage("home");
      console.log("home!!",this);
    });
    $("#mn-mypage").on('click',()=> {
      SceneManager.getInstance().changePage("mypage");
      console.log("mypage!!")
    });
    $("#mn-5c").on('click',()=> {
      console.log("5C!!")
      SceneManager.getInstance().changePage("5c");
    });
    $("#mn-voice").on('click',()=> {
      console.log("voice!!")
      SceneManager.getInstance().changePage("voice");
    });

    $(".prev-arw").on('click',()=> {
      SceneManager.getInstance().changePage("home");
      console.log("prev-arw!!",this);
    });

    $("#homeThumbList").on('click',()=> {
      SceneManager.getInstance().clickedHomeThumb();
      console.log("homeThumbList!!");
    });

    
    
  }



}