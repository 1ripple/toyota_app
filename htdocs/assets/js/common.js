/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	var _CommonUi = __webpack_require__(1);

	var _CommonUi2 = _interopRequireDefault(_CommonUi);

	var _SceneManager = __webpack_require__(2);

	var _SceneManager2 = _interopRequireDefault(_SceneManager);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	$(document).ready(function () {

	  _CommonUi2.default.getInstance().init();
	  _SceneManager2.default.getInstance().init();
	});

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _SceneManager = __webpack_require__(2);

	var _SceneManager2 = _interopRequireDefault(_SceneManager);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var _instance = null;

	var CommonUi = function () {
	  function CommonUi() {
	    _classCallCheck(this, CommonUi);

	    if (_instance !== null) {
	      throw new Error('SingleTon.instance()');
	    }
	    if (_instance === null) {
	      _instance = this;
	    }

	    return _instance;
	  }

	  _createClass(CommonUi, [{
	    key: "init",
	    value: function init() {
	      this.setUpMainNavi();
	    }
	  }, {
	    key: "setUpMainNavi",
	    value: function setUpMainNavi() {
	      var _this = this;

	      $("#mn-home").on('click', function () {
	        _SceneManager2.default.getInstance().changePage("home");
	        console.log("home!!", _this);
	      });
	      $("#mn-mypage").on('click', function () {
	        _SceneManager2.default.getInstance().changePage("mypage");
	        console.log("mypage!!");
	      });
	      $("#mn-5c").on('click', function () {
	        console.log("5C!!");
	        _SceneManager2.default.getInstance().changePage("5c");
	      });
	      $("#mn-voice").on('click', function () {
	        console.log("voice!!");
	        _SceneManager2.default.getInstance().changePage("voice");
	      });

	      $(".prev-arw").on('click', function () {
	        _SceneManager2.default.getInstance().changePage("home");
	        console.log("prev-arw!!", _this);
	      });

	      $("#homeThumbList").on('click', function () {
	        _SceneManager2.default.getInstance().clickedHomeThumb();
	        console.log("homeThumbList!!");
	      });
	    }
	  }], [{
	    key: "getInstance",
	    value: function getInstance() {
	      // this._this = this;
	      if (_instance === null) {
	        _instance = new CommonUi();
	      }
	      return _instance;
	    }
	  }]);

	  return CommonUi;
	}();

	exports.default = CommonUi;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var _instance = null;

	var SceneManager = function () {
	  // static evt = null;
	  function SceneManager() {
	    _classCallCheck(this, SceneManager);

	    Object.defineProperty(this, "prevPageID", {
	      enumerable: true,
	      writable: true,
	      value: "home"
	    });
	    Object.defineProperty(this, "currentPageID", {
	      enumerable: true,
	      writable: true,
	      value: "home"
	    });

	    if (_instance !== null) {
	      throw new Error('SingleTon.instance()');
	    }
	    if (_instance === null) {
	      _instance = this;
	    }

	    return _instance;
	  }

	  _createClass(SceneManager, [{
	    key: "init",
	    value: function init() {}
	  }, {
	    key: "changePage",
	    value: function changePage(pageID) {
	      this.prevPageID = this.currentPageID;
	      this.currentPageID = pageID;

	      TweenMax.to('.scene', 0.3, { display: "none", alpha: 0, ease: Sine.easeOut, delay: 0 });
	      TweenMax.to('#' + pageID, 0.5, { alpha: 1, display: "block", ease: Sine.easeOut, delay: 0.3 });
	    }
	  }, {
	    key: "clickedHomeThumb",
	    value: function clickedHomeThumb() {
	      TweenMax.to('#home', 0.3, { display: "none", alpha: 0, ease: Sine.easeOut, delay: 0 });
	      TweenMax.to('#article', 0.5, { alpha: 1, display: "block", ease: Sine.easeOut, delay: 0.3 });
	    }
	  }], [{
	    key: "getInstance",
	    value: function getInstance() {
	      if (_instance === null) {
	        _instance = new SceneManager();
	      }
	      return _instance;
	    }
	  }]);

	  return SceneManager;
	}();

	exports.default = SceneManager;

/***/ })
/******/ ]);